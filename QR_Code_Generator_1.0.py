# This script will turn a URL into a QR image

import qrcode
from matplotlib import pyplot as plt
from matplotlib import image as mpimg

# Input URL
qr_data = input('Enter a URL to convert to a QR image: ')

# Create an image
img = qrcode.make(qr_data)

# Save as PNG
img.save('qr_png.png')

# Format and display
plt.title('QR Code:')
# Input PNG object
image = mpimg.imread('qr_png.png')
# Set pyplot object
qr = plt.imshow(image)
# Remove axes scale
qr.axes.get_xaxis().set_visible(False)
qr.axes.get_yaxis().set_visible(False)
# Display QR image
plt.show()
